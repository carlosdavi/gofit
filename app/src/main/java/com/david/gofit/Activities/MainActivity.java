package com.david.gofit.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import com.david.gofit.Adapters.ViewPagerAdapter;
import com.david.gofit.Asynks.AsyncHttpConnectionUsers;
import com.david.gofit.DB.BancoDados;
import com.david.gofit.Entities.Aluno;
import com.david.gofit.Entities.Exercicio;
import com.david.gofit.Entities.Refeicao;
import com.david.gofit.Entities.Usuarios;
import com.david.gofit.Enums.Tipo;
import com.david.gofit.Fragments.FragmentDieta;
import com.david.gofit.Fragments.FragmentExercicios;
import com.david.gofit.Interfaces.InterfaceUsuarios;
import com.david.gofit.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements InterfaceUsuarios {
    private Toolbar toolbar;
    private Toolbar toolbarBottom;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private BancoDados bancoDados = new BancoDados();
    private DatabaseReference userReference;
    private FirebaseAuth firebaseAuth;
    private ArrayList<ArrayList<Exercicio>> treino = new ArrayList<>();
    private ArrayList<ArrayList<Refeicao>> dieta = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firebaseAuth = FirebaseAuth.getInstance();
        BancoDados dba = new BancoDados();
        if(firebaseAuth.getCurrentUser() != null){
            Log.e("Usuario Logado",firebaseAuth.getCurrentUser().getEmail());
            BancoDados db = new BancoDados();
            userReference =  db.getNoUsuarios().child(firebaseAuth.getCurrentUser().getUid());

            userReference.addChildEventListener(new ChildEventListener() {

                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String s) {
                    Log.e("FIREBASE", dataSnapshot.getValue().toString());
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

         //   userReference.child("tre
        }
        else
            Log.e("Usuario Não logado:","Usuário NÃO LOGADO ");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        toolbarBottom = (Toolbar) findViewById(R.id.toolbarBottom);

        toolbarBottom.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
             if(item.getItemId() == R.id.action_dieta)
                 configViewPager(viewPager,Tipo.DIETAS);
             else if(item.getItemId() == R.id.action_treino)
                 configViewPager(viewPager,Tipo.EXERCICIOS);
                return true;
            }
        });
        toolbarBottom.inflateMenu(R.menu.menu_bottom);
      //  configViewPager(viewPager,Tipo.EXERCICIOS);

        //bancoDados.getNoUsuarios().child("003").setValue(new UsuariosDB("Beltrano","1234","N"));
      //  bancoDados.getNoAlunos().push().setValue(new Aluno("Aluno1","Souza","Emagrecer",90));
        /*     FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }); */
        getUserData();

    }

    private void getUserData(){
        AsyncHttpConnectionUsers async = new AsyncHttpConnectionUsers(MainActivity.this,"https://gofit2.firebaseio.com/usuarios.json",this);
        async.execute();
    }

    private void configViewPager(ViewPager viewPager, Enum tipo) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        if(tipo == Tipo.EXERCICIOS){
            FragmentExercicios fragSegunda  = new FragmentExercicios();
            fragSegunda.setExercicios(treino.get(0));
            viewPagerAdapter.addFragments(fragSegunda, "Segunda");
            FragmentExercicios fragTerca  = new FragmentExercicios();
            fragTerca.setExercicios(treino.get(1));
            viewPagerAdapter.addFragments(fragTerca, "Terça");
            FragmentExercicios fragQuarta  = new FragmentExercicios();
            fragQuarta.setExercicios(treino.get(2));
            viewPagerAdapter.addFragments(fragQuarta, "Quarta");
            FragmentExercicios fragQuinta  = new FragmentExercicios();
            fragQuinta.setExercicios(treino.get(3));
            viewPagerAdapter.addFragments(fragQuinta, "Quinta");
            FragmentExercicios fragSexta  = new FragmentExercicios();
            fragSexta.setExercicios(treino.get(4));
            viewPagerAdapter.addFragments(fragSexta, "Sexta");
            FragmentExercicios fragSabado  = new FragmentExercicios();
            fragSabado.setExercicios(treino.get(5));
            viewPagerAdapter.addFragments(fragSabado, "Sábado");
        }
        else if (tipo == Tipo.DIETAS) {
            FragmentDieta fragSegunda = new FragmentDieta();
            fragSegunda.setRefeicoes(dieta.get(0));
            viewPagerAdapter.addFragments(fragSegunda, "Segunda");
            FragmentDieta fragTerca = new FragmentDieta();
            fragTerca.setRefeicoes(dieta.get(1));
            viewPagerAdapter.addFragments(fragTerca, "Terça");
            FragmentDieta fragQuarta = new FragmentDieta();
            fragQuarta.setRefeicoes(dieta.get(2));
            viewPagerAdapter.addFragments(fragQuarta, "Quarta");
            FragmentDieta fragQuinta = new FragmentDieta();
            fragQuinta.setRefeicoes(dieta.get(3));
            viewPagerAdapter.addFragments(fragQuinta, "Quinta");
            FragmentDieta fragSexta = new FragmentDieta();
            fragSexta.setRefeicoes(dieta.get(4));
            viewPagerAdapter.addFragments(fragSexta, "Sexta");
            FragmentDieta fragSabado = new FragmentDieta();
            fragSabado.setRefeicoes(dieta.get(5));
            viewPagerAdapter.addFragments(fragSabado, "Sábado");
        }
        viewPager.setAdapter(viewPagerAdapter);
    }

    public void LoadLinksBottomBar() {

    }
    public void CarregarDados(Usuarios usuario){
        ArrayList<Exercicio> segunda = new ArrayList<>();
        ArrayList<Exercicio> terca = new ArrayList<>();
        ArrayList<Exercicio> quarta = new ArrayList<>();
        ArrayList<Exercicio> quinta = new ArrayList<>();
        ArrayList<Exercicio> sexta = new ArrayList<>();
        ArrayList<Exercicio> sabado = new ArrayList<>();
        ArrayList<Refeicao> refSegunda = new ArrayList<>();
        ArrayList<Refeicao> refTerca = new ArrayList<>();
        ArrayList<Refeicao> refQuarta = new ArrayList<>();
        ArrayList<Refeicao> refQuinta = new ArrayList<>();
        ArrayList<Refeicao> refSexta = new ArrayList<>();
        ArrayList<Refeicao> refSabado = new ArrayList<>();
        for(Exercicio ex : usuario.getTreino()){
            if(ex.getDia().equals("segunda"))
                segunda.add(ex);
            else if(ex.getDia().equals("terca"))
                terca.add(ex);
            else if(ex.getDia().equals("quarta"))
                quarta.add(ex);
            else if(ex.getDia().equals("quinta"))
                quinta.add(ex);
            else if(ex.getDia().equals("sexta"))
                sexta.add(ex);
            else if(ex.getDia().equals("sabado"))
                sabado.add(ex);
        }
        for(Refeicao ref : usuario.getDieta()){
            if(ref.getDia().equals("segunda"))
                refSegunda.add(ref);
            else if(ref.getDia().equals("terca"))
                refTerca.add(ref);
            else if(ref.getDia().equals("quarta"))
                refQuarta.add(ref);
            else if(ref.getDia().equals("quinta"))
                refQuinta.add(ref);
            else if(ref.getDia().equals("sexta"))
                refSexta.add(ref);
            else if(ref.getDia().equals("sabado"))
                refSabado.add(ref);


        }

        treino.add(segunda);
        treino.add(terca);
        treino.add(quarta);
        treino.add(quinta);
        treino.add(sexta);
        treino.add(sabado);

        dieta.add(refSegunda);
        dieta.add(refTerca);
        dieta.add(refQuarta);
        dieta.add(refQuinta);
        dieta.add(refSexta);
        dieta.add(refSabado);
        configViewPager(viewPager,Tipo.EXERCICIOS);
}

    public Usuarios getUsuarioCorrente(ArrayList<Usuarios> usuarios){
        for(Usuarios user : usuarios){
            if(user.getUid().equals(firebaseAuth.getUid())){
                return user;
            }
        }
        return null;
    }

    @Override
    public void AfterDownloadUsuarios(ArrayList<Usuarios> listUsuarios) {
        CarregarDados(getUsuarioCorrente(listUsuarios));
    }
}



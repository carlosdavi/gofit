package com.david.gofit.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> listFragments = new ArrayList<>();
        private List<String> titlesTabs = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager fm){
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        return listFragments.get(position);
    }

    @Override
    public int getCount() {
        return listFragments.size();
    }
    public void addFragments(Fragment fragment, String title){
        listFragments.add(fragment);
        titlesTabs.add(title);
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return titlesTabs.get(position);
    }
}

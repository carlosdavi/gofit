package com.david.gofit.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.david.gofit.Entities.Exercicio;
import com.david.gofit.Entities.Refeicao;
import com.david.gofit.Enums.Tipo;
import com.david.gofit.R;

import java.util.ArrayList;

public class AdapterDieta extends BaseAdapter {
    private Context context;
    private ArrayList<Refeicao> refeicoes;

    public AdapterDieta(Context context, ArrayList<Refeicao> refeicoes){
        this.context = context;
        this.refeicoes = refeicoes;
    }
    @Override
    public int getCount() {
        return refeicoes.size();
    }

    @Override
    public Object getItem(int i) {
        return refeicoes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Refeicao refeicao = refeicoes.get(i);
        View layout;

        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = inflater.inflate(R.layout.adapter_dieta,null);
        }
        else{
            layout = view;
        }

        TextView tvRefeicaoTitle = layout.findViewById(R.id.tvTituloRefeicao);
        TextView tvDescricaoRefeicao = layout.findViewById(R.id.tvDescricaoRefeicao);
        TextView tvHorario = layout.findViewById(R.id.tvHorarioRefeicao);
        ImageView imv = layout.findViewById(R.id.tvRefeicaoIcone);
        CarregaIcone(refeicao,imv);


        tvRefeicaoTitle.setText(refeicao.getTitulo());
        tvDescricaoRefeicao.setText(refeicao.getDescricao());
        tvHorario.setText(refeicao.getHorario());

        return layout;
    }
        public void CarregaIcone(Refeicao refeicao, ImageView img){
            //TODO Procurar o icone para "CAFE DA TARDE"
            Tipo tipo = refeicao.getTipo();
            if(tipo == Tipo.CAFEMANHA)
                img.setImageResource(R.mipmap.cafemanha);
            else if(tipo == Tipo.ALMOCO)
                img.setImageResource(R.mipmap.almoco18x18);
            else if(tipo == Tipo.CAFETARDE)
                img.setImageResource(R.mipmap.cafemanha);
            else if(tipo == Tipo.JANTAR)
                img.setImageResource(R.mipmap.jantar18x18);
            else if(tipo == Tipo.CEIA)
                img.setImageResource(R.mipmap.geladeira);

            else{}
        }


}

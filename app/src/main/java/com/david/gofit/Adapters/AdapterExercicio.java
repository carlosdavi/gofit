package com.david.gofit.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.david.gofit.Entities.Exercicio;
import com.david.gofit.R;

import java.util.ArrayList;

public class AdapterExercicio extends BaseAdapter {
    private Context context;
    private ArrayList<Exercicio> exercicios;

    public AdapterExercicio(Context context, ArrayList<Exercicio> exercicios){
        this.context = context;
        this.exercicios = exercicios;
    }
    @Override
    public int getCount() {
        return exercicios.size();
    }

    @Override
    public Object getItem(int i) {
        return exercicios.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Exercicio exercicio = exercicios.get(i);
        View layout;

        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            layout = inflater.inflate(R.layout.adapter_exercicios,null);
        }
        else{
            layout = view;
        }

        TextView tvExercicioTitle = layout.findViewById(R.id.tvTituloExercicio);
        TextView tvDescricaoExercicio = layout.findViewById(R.id.tvDescricaoExercicio);
        TextView textViewSerie = layout.findViewById(R.id.tvSeries);
        TextView tvQuantidade = layout.findViewById(R.id.tvQuantidade);
        // implements img
        tvExercicioTitle.setText(exercicio.getNome());
        tvDescricaoExercicio.setText(tvDescricaoExercicio.getText()+exercicio.getGrupo());
        textViewSerie.setText(exercicio.getSeries());
        tvQuantidade.setText(exercicio.getRepeticoes());
        return layout;
    }
}

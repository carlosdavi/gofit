package com.david.gofit.Asynks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.david.gofit.Entities.Usuarios;
import com.david.gofit.Interfaces.InterfaceUsuarios;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


/**
 * Created by Carlos David on 05/03/2019.
 */

public class AsyncHttpConnectionUsers extends AsyncTask<String, Void, ArrayList<Usuarios>> {
    private  ProgressDialog progressDialog;
    private Context context;
    private String url;
    private InterfaceUsuarios interfaceNews;

     //   private final News news;

   /*     public AsyncHttpConnectionNews(News news) {
            this.news = news;
        }*/

   public AsyncHttpConnectionUsers(Context context, String url, InterfaceUsuarios interfaceNews){
       this.context = context;
       this.url = url;
       this.interfaceNews = interfaceNews;
   }

 @Override
        protected  void onPreExecute(){
            progressDialog = new ProgressDialog(this.context);
            progressDialog.setMessage("Carregando...");
            progressDialog.show();
        }
        @Override
        protected ArrayList<Usuarios> doInBackground(String... params) {
        //   String urlString = params[0];
           StringBuilder content = new StringBuilder();


          //  if (this.news != null) {
                try {
                    URL url = new URL(this.url);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");
                    con.setDoInput(true);
                    con.connect();
                    InputStream inp = con.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inp));

                    String line;
                    while((line = bufferedReader.readLine())!= null){
                        content.append(line + "\n");
                    }
                    bufferedReader.close();

                }
                    catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

          //  }
            ArrayList<Usuarios> listUsuariosReturned;
            Gson gson = new Gson();
              String json =  content.toString();
            Log.e("Mensagem", "AsyncTask Usuarios");

            Type collectionType = new TypeToken<ArrayList<Usuarios>>(){}.getType();
            ArrayList<Usuarios> list = gson.fromJson(json, collectionType);
            Log.e("Mensagem:     ",json );


            return list;//new Gson().fromJson(resposta.toString(), News.class);
        }
        public void teste(){

        }

    @Override
    public void onPostExecute(ArrayList<Usuarios> newsList){
           //super.onPostExecute(newsList);
        this.progressDialog.setMessage("Carregado!");
        interfaceNews.AfterDownloadUsuarios(newsList);
        this.progressDialog.dismiss();

        }
    }


package com.david.gofit.Entities;

public class Aluno {

    private String nome;
    private String sobreNome;
    private String objetivo;
    private double peso;
    private Treino treino;


    public Aluno(String nome, String sobreNome, String objetivo, double peso, Treino treino) {
        this.nome = nome;
        this.sobreNome = sobreNome;
        this.objetivo = objetivo;
        this.peso = peso;
        this.treino = treino;
    }

    public Aluno(String nome, String sobreNome, String objetivo, double peso) {
        this.nome = nome;
        this.sobreNome = sobreNome;
        this.objetivo = objetivo;
        this.peso = peso;
    }


    public Treino getTreino() {
        return treino;
    }

    public void setTreino(Treino treino) {
        this.treino = treino;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobreNome() {
        return sobreNome;
    }

    public void setSobreNome(String sobreNome) {
        this.sobreNome = sobreNome;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }
}

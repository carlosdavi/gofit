package com.david.gofit.Entities;

public class Exercicio {

    private String dia;
    private String nome;
    private String grupo;
    private String sigla;
    private String series;
    private String repeticoes;

    public Exercicio(String nome, String grupo, String sigla, String series, String repeticoes) {
        this.nome = nome;
        this.grupo = grupo;
        this.sigla = sigla;
        this.series = series;
        this.repeticoes = repeticoes;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getRepeticoes() {
        return repeticoes;
    }

    public void setRepeticoes(String repeticoes) {
        this.repeticoes = repeticoes;
    }
}

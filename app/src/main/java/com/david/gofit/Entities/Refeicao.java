package com.david.gofit.Entities;

import com.david.gofit.Enums.Tipo;

public class Refeicao {

    private String dia;
    private String titulo;
    private Tipo tipo;
    private String descricao;
    private String horario;

    public Refeicao(String titulo, Tipo tipo,String descricao, String horario){
        this.titulo = titulo;
        this.tipo = tipo;
        this.descricao = descricao;
        this.horario = horario;
    }


    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}

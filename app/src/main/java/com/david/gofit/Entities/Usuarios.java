package com.david.gofit.Entities;

import java.util.ArrayList;

public class Usuarios {

    private String nomeUser;
    private String uid;
    private String verificadoNutri;
    private String verificadoPersonal;
    private String tipoUser;
    private ArrayList<Refeicao> dieta;
    private ArrayList<Exercicio> treino;

    public Usuarios(String verificadoNutri, String verificadoPersonal, String tipoUser) {
        this.verificadoNutri = verificadoNutri;
        this.verificadoPersonal = verificadoPersonal;
        this.tipoUser = tipoUser;
    }

    public String getNomeUser() {
        return nomeUser;
    }

    public void setNomeUser(String nomeUser) {
        this.nomeUser = nomeUser;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getVerificadoNutri() {
        return verificadoNutri;
    }

    public void setVerificadoNutri(String verificadoNutri) {
        this.verificadoNutri = verificadoNutri;
    }

    public String getVerificadoPersonal() {
        return verificadoPersonal;
    }

    public void setVerificadoPersonal(String verificadoPersonal) {
        this.verificadoPersonal = verificadoPersonal;
    }

    public String getTipoUser() {
        return tipoUser;
    }

    public void setTipoUser(String tipoUser) {
        this.tipoUser = tipoUser;
    }

    public ArrayList<Refeicao> getDieta() {
        return dieta;
    }

    public void setDieta(ArrayList<Refeicao> dieta) {
        this.dieta = dieta;
    }

    public ArrayList<Exercicio> getTreino() {
        return treino;
    }

    public void setTreino(ArrayList<Exercicio> treino) {
        this.treino = treino;
    }
}
package com.david.gofit.DB;

import android.provider.ContactsContract;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class BancoDados {

    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference userReference = databaseReference.child("usuarios");
    private DatabaseReference treinoReference = databaseReference.child("usuarios").child("treinos");

    public DatabaseReference getNoUsuarios() {
        return this.userReference;
    }
    public DatabaseReference getNoTreino(){
        return this.treinoReference;
    }

    public DatabaseReference getReferenceDB(){
        return this.databaseReference;
    }

}


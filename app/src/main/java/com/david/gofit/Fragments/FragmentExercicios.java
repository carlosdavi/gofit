package com.david.gofit.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.david.gofit.Adapters.AdapterExercicio;
import com.david.gofit.Entities.Exercicio;
import com.david.gofit.Interfaces.InterfaceExercicios;
import com.david.gofit.R;

import java.util.ArrayList;

public class FragmentExercicios extends Fragment implements InterfaceExercicios {

    private View layout;
    private TextView tvDate;
    private ArrayList<Exercicio> exercicios = new ArrayList<>();




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onDestroyView (){
        super.onDestroyView();
        Log.e("Mensagem", "FRAGMENT CALENDAR - ONDESTROY()");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       layout = inflater.inflate(R.layout.fragment_exercicios,container,false);
        Log.e("Mensagem:","Chamando Fragment Calendario");
       // AsyncHttpConnectionCalendar asyncHttpConnectionCalendar = new AsyncHttpConnectionCalendar(getContext(),"http://mism12.kinghost.net:21001/calendar",this);
        //asyncHttpConnectionCalendar.execute();
       // LoadExerciciosTeste();
       // if(exercicios != null || exercicios.size() ==0 )

            LoadExercicios(exercicios);


        return  layout;
    }

    private void LoadExerciciosTeste(){

        ListView lv = this.layout.findViewById(R.id.lvExerciciosDia);
        Exercicio aq1 = new Exercicio("Corrida","AQU","SP R.","A","12MIN");
        Exercicio ex1 = new Exercicio("Supino Reto","A","SP R.","3X","12");
        Exercicio ex2 = new Exercicio("Supino Inclinado 45","A","SP R.","3X","12");
        Exercicio ex3 = new Exercicio("Apoio de Frente","A","Ap. F.","3X","12");
        Exercicio ex4 = new Exercicio("Fly","A","Fly","3X","12");
        Exercicio ex5 = new Exercicio("Peck Deck","A","P. D","3X","12");
        Exercicio ex6 = new Exercicio("Abdominal","A","AB","3X","12");
        Exercicio aq2 = new Exercicio("Eliptico","AQU","SP R.","A","20MIN");
        exercicios = new ArrayList<>();
        exercicios.add(aq1);
        exercicios.add(ex1);
        exercicios.add(ex2);
        exercicios.add(ex3);
        exercicios.add(ex4);
        exercicios.add(ex5);
        exercicios.add(ex6);
        exercicios.add(aq2);

        lv.setAdapter(new AdapterExercicio(getContext(),exercicios));

    }
    private void LoadExercicios(ArrayList<Exercicio> listExercicios){
        this.exercicios = listExercicios;
        ListView lv = this.layout.findViewById(R.id.lvExerciciosDia);

        lv.setAdapter(new AdapterExercicio(getContext(),listExercicios));

    }

    public ArrayList<Exercicio> getExercicios() {
        return exercicios;
    }

    public void setExercicios(ArrayList<Exercicio> exercicios) {
        this.exercicios = exercicios;
    }

    @Override
    public void AfterDownloadExercicios(ArrayList<Exercicio> listExercicios) {
        this.exercicios = listExercicios;
        LoadExercicios(listExercicios);
    }
}


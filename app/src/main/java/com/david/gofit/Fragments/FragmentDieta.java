package com.david.gofit.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.david.gofit.Adapters.AdapterDieta;
import com.david.gofit.Entities.Refeicao;
import com.david.gofit.Enums.Tipo;
import com.david.gofit.Interfaces.InterfaceDieta;
import com.david.gofit.R;

import java.util.ArrayList;

public class FragmentDieta extends Fragment implements InterfaceDieta {

    private View layout;
    private TextView tvDate;

    private ArrayList<Refeicao> refeicoes = new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onDestroyView (){
        super.onDestroyView();
        Log.e("Mensagem", "FRAGMENT CALENDAR - ONDESTROY()");
    }

    public ArrayList<Refeicao> getRefeicoes() {
        return refeicoes;
    }

    public void setRefeicoes(ArrayList<Refeicao> refeicoes) {
        this.refeicoes = refeicoes;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.fragment_exercicios,container,false);
        Log.e("Mensagem:","Chamando Fragment Calendario");
        // AsyncHttpConnectionCalendar asyncHttpConnectionCalendar = new AsyncHttpConnectionCalendar(getContext(),"http://mism12.kinghost.net:21001/calendar",this);
        //asyncHttpConnectionCalendar.execute();
        //LoadRefeicoesTeste();
        LoadRefeicaoWeb(refeicoes);

        return  layout;
    }

    private void LoadRefeicoesTeste(){

        ListView lv = this.layout.findViewById(R.id.lvExerciciosDia);
        Refeicao br = new Refeicao("Café da Manhã", Tipo.CAFEMANHA,"200g de queijo com café","07:00");
        Refeicao al = new Refeicao("Almoço", Tipo.ALMOCO,"200g de queijo com café","12:00");
        Refeicao br2 = new Refeicao("Café da Tarde", Tipo.CAFETARDE,"200g de queijo com café","15:00");
        Refeicao jan = new Refeicao("Jantar", Tipo.JANTAR,"200g de queijo com café","20:00");
        Refeicao cei = new Refeicao("Ceia", Tipo.CEIA,"200g de queijo com café","22:00");

        refeicoes = new ArrayList<>();
        refeicoes.add(br);
        refeicoes.add(al);
        refeicoes.add(br2);
        refeicoes.add(jan);
        refeicoes.add(cei);


        lv.setAdapter(new AdapterDieta(getContext(),refeicoes));

    }
    private void LoadRefeicaoWeb(ArrayList<Refeicao> listRefeicao){
        this.refeicoes = listRefeicao;
        ListView lv = this.layout.findViewById(R.id.lvExerciciosDia);

        lv.setAdapter(new AdapterDieta(getContext(),refeicoes));

    }



    @Override
    public void AfterDownloadDieta(ArrayList<Refeicao> listRefeicao) {
        this.refeicoes = listRefeicao;
        LoadRefeicaoWeb(listRefeicao);
    }
}

